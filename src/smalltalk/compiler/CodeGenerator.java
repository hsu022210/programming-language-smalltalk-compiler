package smalltalk.compiler;

import org.antlr.symtab.*;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.TerminalNode;
import smalltalk.compiler.symbols.*;

import java.util.List;

/** Fill STBlock, STMethod objects in Symbol table with bytecode,
 * {@link STCompiledBlock}.
 */
public class CodeGenerator extends SmalltalkBaseVisitor<Code> {
	public static final boolean dumpCode = false;

	public STClass currentClassScope;
	public Scope currentScope;

	/** With which compiler are we generating code? */
	public final Compiler compiler;

	public CodeGenerator(Compiler compiler) {
		this.compiler = compiler;
	}

	/** This and defaultResult() critical to getting code to bubble up the
	 *  visitor call stack when we don't implement every method.
	 */
	@Override
	protected Code aggregateResult(Code aggregate, Code nextResult) {
		if ( aggregate!=Code.None ) {
			if ( nextResult!=Code.None ) {
				return aggregate.join(nextResult);
			}
			return aggregate;
		}
		else {
			return nextResult;
		}
	}

	@Override
	protected Code defaultResult() {
		return Code.None;
	}

	@Override
	public Code visitFile(SmalltalkParser.FileContext ctx) {
		currentScope = compiler.symtab.GLOBALS;
		return visitChildren(ctx);
	}

	@Override
	public Code visitClassDef(SmalltalkParser.ClassDefContext ctx) {
		currentClassScope = ctx.scope;
		pushScope(ctx.scope);
		Code code = visitChildren(ctx);
		popScope();
		currentClassScope = null;
		return code;
	}

	public STCompiledBlock getCompiledPrimitive(STPrimitiveMethod primitive) {
		STCompiledBlock compiledMethod = new STCompiledBlock(currentClassScope, primitive);
		return compiledMethod;
	}

	/**
	 All expressions have values. Must pop each expression value off, except
	 last one, which is the block return value. Visit method for blocks will
	 issue block_return instruction. Visit method for method will issue
	 pop self return.  If last expression is ^expr, the block_return or
	 pop self return is dead code but it is always there as a failsafe.

	 localVars? expr ('.' expr)* '.'?
	 */
	@Override
	public Code visitFullBody(SmalltalkParser.FullBodyContext ctx) {
		Code code = defaultResult();
		if (ctx.localVars() != null){
			 code = aggregateResult(code, visit(ctx.localVars()));
		}

		for (int i = 0; i < ctx.stat().size(); i++){
			code = aggregateResult(code, visit(ctx.stat(i)));
			if (i != ctx.stat().size()-1){
				code = aggregateResult(code, Compiler.pop());
			}
		}
		return code;
	}

	@Override
	public Code visitEmptyBody(SmalltalkParser.EmptyBodyContext ctx) {
		Code code = visitChildren(ctx);
		if (currentScope instanceof STMethod){
			code = aggregateResult(code, Compiler.push_self());
			code = aggregateResult(code, Compiler.method_return());
		} else {
			code = aggregateResult(code, Compiler.push_nil());
		}
		return code;
	}

	@Override
	public Code visitLiteral(SmalltalkParser.LiteralContext ctx) {
		Code code = visitChildren(ctx);
		String s = ctx.getText();

		if (ctx.NUMBER() != null){
			code = aggregateResult(code, Compiler.push_int(Integer.valueOf(s)));
		} else if (ctx.STRING() != null){
			s = s.replace("\'", "");
			code = aggregateResult(code, Compiler.push_literal(getLiteralIndex(s)));
		} else {
			switch (s){
				case "nil":
					code = aggregateResult(code, Compiler.push_nil());
					break;
				case "false":
					code = aggregateResult(code, Compiler.push_false());
					break;
				case "true":
					code = aggregateResult(code, Compiler.push_true());
					break;
				case "self":
					code = aggregateResult(code, Compiler.push_self());
					break;
				default:
					break;
			}
		}
		return code;
	}

	@Override
	public Code visitId(SmalltalkParser.IdContext ctx) {
		Code code = visitChildren(ctx);

		if (ctx.sym instanceof FieldSymbol){
			int num = ((STBlock)currentScope).getLocalIndex(ctx.sym.getName());
			code = aggregateResult(code, Compiler.push_field(num));
		} else if (ctx.sym instanceof VariableSymbol){
			int x, y;
			x = ((STBlock)currentScope).getRelativeScopeCount(ctx.sym.getScope().getName());
			y = ctx.sym.getInsertionOrderNumber();
			code = aggregateResult(code, Compiler.push_local(x, y));
		} else {
			code = aggregateResult(code, Compiler.push_global(getLiteralIndex(ctx.getText())));
		}
		return code;
	}

	public int getFieldIndex(Symbol sym) {
		int num = ((STClass)sym.getScope()).getFieldIndex(sym.getName());
		ClassSymbol scope = ((STClass) sym.getScope()).getSuperClassScope();
		while (scope != null){
			num += scope.getNumberOfDefinedFields();
			scope = scope.getSuperClassScope();
		}
		return num;
	}

	@Override
	public Code visitMain(SmalltalkParser.MainContext ctx) {
		currentClassScope = ctx.classScope;
		Code code = defaultResult();
		if (ctx.classScope != null){
			pushScope(ctx.scope);
			code = visitChildren(ctx);
			code = aggregateResult(code, Compiler.pop());
			code = aggregateResult(code, Compiler.push_self());
			code = aggregateResult(code, Compiler.method_return());
			ctx.scope.compiledBlock = getCompiledBlockAndSetByte(ctx.scope, code);
			popScope();
		}
		currentClassScope = null;
		return code;
	}

	@Override
	public Code visitBinaryExpression(SmalltalkParser.BinaryExpressionContext ctx) {
		Code code = visit(ctx.unaryExpression(0));
		if (ctx.unaryExpression().size() >= 2) {
			for (int i = 1; i < ctx.unaryExpression().size(); i++){
				code = aggregateResult(code, visit(ctx.unaryExpression(i)));
				code = aggregateResult(code, visit(ctx.bop(i-1)));
			}
		}
		return code;
	}

	@Override
	public Code visitAssign(SmalltalkParser.AssignContext ctx) {
		Code code = visit(ctx.messageExpression());
		code = aggregateResult(code, visit(ctx.lvalue()));
		return code;
	}

	@Override
	public Code visitLvalue(SmalltalkParser.LvalueContext ctx) {
		Code code = visitChildren(ctx);
		if (ctx.sym instanceof FieldSymbol){
			code = aggregateResult(code, Compiler.store_field(ctx.sym.getInsertionOrderNumber()));
		} else {
			int x, y;
			x = ((STBlock)currentScope).getRelativeScopeCount(ctx.sym.getScope().getName());
			y = ctx.sym.getInsertionOrderNumber();
			code = aggregateResult(code, Compiler.store_local(x, y));
		}
		return code;
	}

	@Override
	public Code visitNamedMethod(SmalltalkParser.NamedMethodContext ctx) {
		pushScope(ctx.scope);
		Code code = visitChildren(ctx);
		ctx.scope.compiledBlock = getCompiledBlockAndSetByte(ctx.scope, code);
		popScope();
		return code;
	}

	@Override
	public Code visitOperatorMethod(SmalltalkParser.OperatorMethodContext ctx) {
		pushScope(ctx.scope);
		Code code = visitChildren(ctx);
		ctx.scope.compiledBlock = getCompiledBlockAndSetByte(ctx.scope, code);
		popScope();
		return code;
	}

	@Override
	public Code visitKeywordMethod(SmalltalkParser.KeywordMethodContext ctx) {
		pushScope(ctx.scope);
		Code code = visitChildren(ctx);
		ctx.scope.compiledBlock = getCompiledBlockAndSetByte(ctx.scope, code);
		popScope();
		return code;
	}

	@Override
	public Code visitSmalltalkMethodBlock(SmalltalkParser.SmalltalkMethodBlockContext ctx) {
		Code code = visitChildren(ctx);
		if (ctx.body().getChildCount() > 0){
			code = aggregateResult(code, Compiler.pop());
			code = aggregateResult(code, Compiler.push_self());
			code = aggregateResult(code, Compiler.method_return());
		}
		return code;
	}

	@Override
	public Code visitBlock(SmalltalkParser.BlockContext ctx) {
		pushScope(ctx.scope);
		Code code= aggregateResult(defaultResult(), Compiler.block(ctx.scope.index));
		Code childrenCode = visitChildren(ctx);
		childrenCode = aggregateResult(childrenCode, Compiler.block_return());
		ctx.scope.compiledBlock = getCompiledBlockAndSetByte(ctx.scope, childrenCode);
		popScope();
		return code;
	}

	@Override
	public Code visitBop(SmalltalkParser.BopContext ctx) {
		Code code = visitChildren(ctx);
		if (!ctx.getText().equals(",")){
			code = aggregateResult(code, Compiler.send(1, getLiteralIndex(ctx.getText())));
		}
		return code;
	}

	@Override
	public Code visitKeywordSend(SmalltalkParser.KeywordSendContext ctx) {
		Code code = visitChildren(ctx);
		String s = "";
		for (TerminalNode ke: ctx.KEYWORD()) {
			s += ke.getText();
		}
		code = aggregateResult(code, Compiler.send(ctx.args.size(), getLiteralIndex(s)));
		return code;
	}

	@Override
	public Code visitUnarySuperMsgSend(SmalltalkParser.UnarySuperMsgSendContext ctx) {
		Code code = visitChildren(ctx);
		String s = ctx.ID().getText();
		code = aggregateResult(code, Compiler.push_self());
		code = aggregateResult(code, Compiler.send_super(0, getLiteralIndex(s)));
		return code;
	}

	@Override
	public Code visitUnaryMsgSend(SmalltalkParser.UnaryMsgSendContext ctx) {
		Code code = visitChildren(ctx);
		String s = ctx.ID().getText();
		code = aggregateResult(code, Compiler.send(0, getLiteralIndex(s)));
		return code;
	}

	@Override
	public Code visitReturn(SmalltalkParser.ReturnContext ctx) {
		Code e = visit(ctx.messageExpression());
		if ( compiler.genDbg ) {
			e = Code.join(e, dbg(ctx.start)); // put dbg after expression as that is when it executes
		}
		Code code = e.join(Compiler.method_return());
		return code;
	}

	public void pushScope(Scope scope) {
		currentScope = scope;
	}

	public void popScope() {
//		if ( currentScope.getEnclosingScope()!=null ) {
//			System.out.println("popping from " + currentScope.getScopeName() + " to " + currentScope.getEnclosingScope().getScopeName());
//		}
//		else {
//			System.out.println("popping from " + currentScope.getScopeName() + " to null");
//		}
		currentScope = currentScope.getEnclosingScope();
	}

	private STCompiledBlock getCompiledBlockAndSetByte(STBlock block, Code code){
		STCompiledBlock stCompiledBlock = new STCompiledBlock(currentClassScope, block);
		stCompiledBlock.bytecode = code.bytes();
		return stCompiledBlock;
	}

	public int getLiteralIndex(String s) {
		return currentClassScope.stringTable.add(s);
	}

	public Code dbgAtEndMain(Token t) {
		int charPos = t.getCharPositionInLine() + t.getText().length();
		return dbg(t.getLine(), charPos);
	}

	public Code dbgAtEndBlock(Token t) {
		int charPos = t.getCharPositionInLine() + t.getText().length();
		charPos -= 1; // point at ']'
		return dbg(t.getLine(), charPos);
	}

	public Code dbg(Token t) {
		return dbg(t.getLine(), t.getCharPositionInLine());
	}

	public Code dbg(int line, int charPos) {
		return Compiler.dbg(getLiteralIndex(compiler.getFileName()), line, charPos);
	}
	public Code store(String id) {
		return null;
	}

	public Code push(String id) {
		return null;
	}

	public Code sendKeywordMsg(ParserRuleContext receiver,
							   Code receiverCode,
							   List<SmalltalkParser.BinaryExpressionContext> args,
							   List<TerminalNode> keywords)
	{
		return null;
	}

	public String getProgramSourceForSubtree(ParserRuleContext ctx) {
		return null;
	}
}
